package org.dailycode.pawon.fragment;

import java.util.ArrayList;
import java.util.List;

import org.dailycode.pawon.App;
import org.dailycode.pawon.R;
import org.dailycode.pawon.adapter.ListItemOptionAdapter;
import org.dailycode.pawon.entity.ItemOption;
import org.dailycode.pawon.entity.LocalizedInformation;

import android.app.DialogFragment;
import android.os.Bundle;
import android.os.Parcel;
import android.os.Parcelable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ListView;

public class DialogOptionFragment extends DialogFragment {

	private List<ItemOption> itemOptions = new ArrayList<ItemOption>();
	private ListItemOptionAdapter adapter;

	public DialogOptionFragment() {
		super();
	}

	public DialogOptionFragment(List<ItemOption> itemOptions) {
		this.itemOptions = itemOptions;

	}

	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		// Set false mean we should save state of instance, because the
		// instance never re create if the device in different state like
		// rotating screen
		setRetainInstance(false);
	}

	public View onCreateView(LayoutInflater inflater, ViewGroup container, final Bundle savedInstanceState) {
		getDialog().setTitle("Option Menu");
		getDialog().setCancelable(true);
		View v = inflater.inflate(R.layout.dialog_item_option, container, false);
		
		ListView listView = (ListView) v.findViewById(R.id.listItemOption);
		adapter = new ListItemOptionAdapter(v.getContext(), R.id.textViewTitle, itemOptions);
		listView.setAdapter(adapter);

		if (savedInstanceState != null) {
			List<ItemOptionParcelable> itemOptionParcelables = savedInstanceState.getParcelableArrayList("itemOptions");
			for (ItemOptionParcelable itemOptionParcelable : itemOptionParcelables) {
				itemOptions.add(new ItemOption(itemOptionParcelable.id, itemOptionParcelable.name,
						itemOptionParcelable.parentId, itemOptionParcelable.price,
						itemOptionParcelable.localizedInformation));

			}

			List<ItemOptionParcelable> checkedItemOptionParcelables = savedInstanceState
					.getParcelableArrayList("checkedItemOptions");
			for (ItemOptionParcelable itemOptionParcelable : checkedItemOptionParcelables) {
				adapter.getCheckedtemOptions().add(
						new ItemOption(itemOptionParcelable.id, itemOptionParcelable.name,
								itemOptionParcelable.parentId, itemOptionParcelable.price,
								itemOptionParcelable.localizedInformation));

			}
		}

		Button btnOK = (Button) v.findViewById(R.id.buttonOK);
		btnOK.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				App.getInstance().createCheckedOptions(adapter.getCheckedtemOptions());
				dismiss();

			}
		});
		return v;
	}

	@Override
	public void onDestroyView() {
		if (getDialog() != null && getRetainInstance())
			getDialog().setDismissMessage(null);

		super.onDestroyView();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);

		ArrayList<ItemOptionParcelable> extraItemOption = new ArrayList<ItemOptionParcelable>();
		for (ItemOption itemOption : itemOptions) {
			extraItemOption.add(new ItemOptionParcelable(itemOption.getId(), itemOption.getName(), itemOption
					.getParentId(), itemOption.getPrice(), itemOption.getLocalizedInformation()));
		}

		outState.putParcelableArrayList("itemOptions", extraItemOption);

		ArrayList<ItemOptionParcelable> extraCheckedItemOption = new ArrayList<ItemOptionParcelable>();
		for (ItemOption itemOption : adapter.getCheckedtemOptions()) {
			extraCheckedItemOption.add(new ItemOptionParcelable(itemOption.getId(), itemOption.getName(), itemOption
					.getParentId(), itemOption.getPrice(), itemOption.getLocalizedInformation()));
		}

		outState.putParcelableArrayList("checkedItemOptions", extraCheckedItemOption);

	}

	class ItemOptionParcelable implements Parcelable {

		private Long id;
		private String name;
		private Long parentId;
		private Double price;
		private LocalizedInformation localizedInformation;

		public ItemOptionParcelable(Long id, String name, Long parentId, Double price,
				LocalizedInformation localizedInformation) {
			super();
			this.id = id;
			this.name = name;
			this.parentId = parentId;
			this.price = price;
			this.localizedInformation = localizedInformation;
		}

		public ItemOptionParcelable(Parcel read) {
			id = read.readLong();
			name = read.readString();
			parentId = read.readLong();
			price = read.readDouble();
			localizedInformation = (LocalizedInformation) read.readValue(null);
		}

		@Override
		public int describeContents() {
			return 0;
		}

		@Override
		public void writeToParcel(Parcel dest, int flags) {
			dest.writeLong(id);
			dest.writeString(name);
			dest.writeLong(parentId);
			dest.writeDouble(price);
			dest.writeValue(localizedInformation);
		}

		public final Parcelable.Creator<ItemOptionParcelable> CREATOR = new Parcelable.Creator<ItemOptionParcelable>() {

			@Override
			public ItemOptionParcelable createFromParcel(Parcel source) {
				return new ItemOptionParcelable(source);
			}

			@Override
			public ItemOptionParcelable[] newArray(int size) {
				return new ItemOptionParcelable[size];
			}
		};

		@Override
		public String toString() {
			return "ItemOptionParcelable [id=" + id + ", name=" + name + ", parentId=" + parentId + ", price=" + price
					+ ", localizedInformation=" + localizedInformation.toString() + "]";
		}

	}

}
