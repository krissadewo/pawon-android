package org.dailycode.pawon.service;

import java.net.MalformedURLException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.dailycode.pawon.App;
import org.dailycode.pawon.entity.Item;
import org.dailycode.pawon.entity.ItemCategory;
import org.dailycode.pawon.entity.ItemOption;
import org.dailycode.pawon.entity.Language;
import org.dailycode.pawon.entity.LocalizedInformation;
import org.dailycode.pawon.entity.Purchase;
import org.dailycode.pawon.entity.PurchaseItem;
import org.dailycode.pawon.entity.PurchaseOption;
import org.dailycode.pawon.entity.Room;
import org.dailycode.pawon.helper.JSONParser;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JSONService {

	private static JSONService instance;
	private JSONParser parser = new JSONParser();

	private JSONService() {

	}

	public static JSONService getInstance() {
		if (instance == null) {
			instance = new JSONService();
		}
		return instance;
	}

	/**
	 * 
	 * @return
	 */
	public List<Language> getAllLanguage() {
		try {
			List<Language> languages = new ArrayList<Language>();
			JSONArray jsonArray = parser.getJSONData(App.getInstance().getBaseJSONUrl("language.json").toURL());
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jo = jsonArray.getJSONObject(i);
				Language language = new Language();
				language.setActive(jo.getBoolean("active"));
				if (language.isActive()) {
					language.setId(jo.getLong("id"));
					language.setName(jo.getString("name"));
					languages.add(language);
				}
			}
			return languages;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param name
	 * @return
	 */
	public Room getTableByName(String name) {
		try {
			Room room = new Room();
			JSONArray jsonArray = parser.getJSONData(App.getInstance()
					.getBaseJSONUrl("room/roomName/" + name + ".json").toURL());
			JSONObject jsonObject = jsonArray.getJSONObject(0);
			if (jsonObject == null) {
				room = null;
			} else {
				room.setId(jsonObject.getLong("id"));
				room.setName(jsonObject.getString("name"));
			}
			return room;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param languageId
	 * @return
	 */
	public List<ItemCategory> getCategory(Long languageId) {
		try {
			List<ItemCategory> categories = new ArrayList<ItemCategory>();
			JSONArray jsonArray = parser.getJSONData(App.getInstance()
					.getBaseJSONUrl("itemCategory/section/restaurant/languageId/" + languageId + ".json").toURL());
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jo = jsonArray.getJSONObject(i);
				ItemCategory category = new ItemCategory();
				category.setActive(jo.getBoolean("active"));
				if (category.isActive()) {
					category.setId(jo.getLong("id"));
					category.setParentId(jo.getLong("parentId"));
					category.setName(jo.getString("name"));
					category.setActive(jo.getBoolean("active"));

					LocalizedInformation localizedInformation = new LocalizedInformation();
					localizedInformation.setTitle(jo.getJSONObject("localizedInformation").getString("title"));
					localizedInformation.setDescription(jo.getJSONObject("localizedInformation").getString(
							"description"));
					category.setLocalizedInformation(localizedInformation);

					categories.add(category);
				}
			}
			return categories;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param categoryID
	 * @param languageId
	 * @return
	 */
	public List<ItemCategory> getChildCategory(Long categoryID, Long languageId) {
		try {
			List<ItemCategory> categories = new ArrayList<ItemCategory>();
			JSONArray jsonArray = parser.getJSONData(App.getInstance()
					.getBaseJSONUrl("itemCategory/parentId/" + categoryID + "/languageId/" + languageId + ".json")
					.toURL());
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jo = jsonArray.getJSONObject(i);
				ItemCategory category = new ItemCategory();
				category.setActive(jo.getBoolean("active"));
				if (category.isActive()) {
					category.setId(jo.getLong("id"));
					category.setParentId(jo.getLong("parentId"));
					category.setName(jo.getString("name"));

					LocalizedInformation localizedInformation = new LocalizedInformation();
					localizedInformation.setTitle(jo.getJSONObject("localizedInformation").getString("title"));
					localizedInformation.setDescription(jo.getJSONObject("localizedInformation").getString(
							"description"));
					category.setLocalizedInformation(localizedInformation);
					categories.add(category);
				}
			}
			return categories;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param parentId
	 * @return
	 */
	public boolean isCategoryHasChild(Long parentId) {
		try {
			return parser.getJSONBoolean(App.getInstance()
					.getBaseJSONUrl("itemCategory/hasChild/" + parentId + ".json").toURL());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return false;
	}

	/**
	 * 
	 * @param categoryID
	 * @param languageID
	 * @return
	 */
	public List<Item> getItems(Long categoryID, Long languageID) {
		try {
			List<Item> items = new ArrayList<Item>();
			JSONArray jsonArray = parser.getJSONData(App.getInstance()
					.getBaseJSONUrl("item/categoryId/" + categoryID + "/languageId/" + languageID + ".json").toURL());
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jo = jsonArray.getJSONObject(i);
				Item item = new Item();
				item.setActive(jo.getBoolean("active"));
				if (item.isActive()) {
					item.setId(jo.getLong("id"));
					item.setName(jo.getString("name"));
					item.setImage(jo.getString("image"));
					items.add(item);
				}
			}
			return items;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param itemID
	 * @param languageID
	 * @return
	 */
	public Item getItemByID(Long itemID, Long languageID) {
		try {
			JSONArray jsonArray = parser.getJSONData(App.getInstance()
					.getBaseJSONUrl("item/itemId/" + itemID + "/languageId/" + languageID + ".json").toURL());
			Item item = new Item();
			JSONObject jo = jsonArray.getJSONObject(0);
			item.setActive(jo.getBoolean("active"));
			if (item.isActive()) {
				item.setId(jo.getLong("id"));
				item.setName(jo.getString("name"));
				item.setImage(jo.getString("image"));
				item.setPrice(jo.getDouble("price"));
				item.setDiscount(jo.getDouble("discount"));

				LocalizedInformation localizedInformation = new LocalizedInformation();
				localizedInformation.setTitle(jo.getJSONObject("localizedInformation").getString("title"));
				localizedInformation.setDescription(jo.getJSONObject("localizedInformation").getString("description"));
				item.setLocalizedInformation(localizedInformation);
			}
			return item;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param itemID
	 * @param languageID
	 * @return
	 */
	public List<ItemOption> getItemOptionByItem(Long itemID, Long languageID) {
		try {
			List<ItemOption> itemOptions = new ArrayList<ItemOption>();
			JSONArray jsonArray = parser.getJSONData(App.getInstance()
					.getBaseJSONUrl("option/itemId/" + itemID + "/languageId/" + languageID + ".json").toURL());
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jo = jsonArray.getJSONObject(i);
				ItemOption itemOption = new ItemOption();
				itemOption.setActive(jo.getBoolean("active"));
				if (itemOption.isActive()) {
					itemOption.setId(jo.getLong("id"));
					itemOption.setName(jo.getString("name"));
					itemOption.setPrice(jo.getDouble("price"));
					itemOption.setParentId(jo.getLong("parentId"));

					LocalizedInformation localizedInformation = new LocalizedInformation();
					localizedInformation.setTitle(jo.getJSONObject("localizedInformation").getString("title"));
					localizedInformation.setDescription(jo.getJSONObject("localizedInformation").getString(
							"description"));
					itemOption.setLocalizedInformation(localizedInformation);

					itemOptions.add(itemOption);
				}
			}
			return itemOptions;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	/**
	 * 
	 * @param parentID
	 * @param languageID
	 * @return
	 */
	public List<ItemOption> getItemOptionByParent(Long parentID, Long languageID) {
		try {
			List<ItemOption> itemOptions = new ArrayList<ItemOption>();
			JSONArray jsonArray = parser.getJSONData(App.getInstance()
					.getBaseJSONUrl("option/optionId/" + parentID + "/languageId/" + languageID + ".json").toURL());
			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jo = jsonArray.getJSONObject(i);
				ItemOption itemOption = new ItemOption();
				itemOption.setActive(jo.getBoolean("active"));
				if (itemOption.isActive()) {
					itemOption.setId(jo.getLong("id"));
					itemOption.setName(jo.getString("name"));
					itemOption.setPrice(jo.getDouble("price"));
					itemOption.setParentId(jo.getLong("parentId"));

					LocalizedInformation localizedInformation = new LocalizedInformation();
					localizedInformation.setTitle(jo.getJSONObject("localizedInformation").getString("title"));
					localizedInformation.setDescription(jo.getJSONObject("localizedInformation").getString(
							"description"));
					itemOption.setLocalizedInformation(localizedInformation);

					itemOptions.add(itemOption);
				}
			}
			return itemOptions;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	public Boolean orderPurchase(Long itemID, String options, String roomName, Long languageID) {
		try {
			return parser.getJSONBoolean(App
					.getInstance()
					.getBaseJSONUrl(
							"purchase/itemId/" + itemID + "/options/" + options + "/roomName/" + roomName
									+ "/languageId/" + languageID + ".json").toURL());
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return false;
	}

	public Purchase getPurchaseByRoom(String roomName) {
		try {
			JSONArray jsonArray = parser.getJSONData(App.getInstance()
					.getBaseJSONUrl("purchase/show/roomName/" + roomName + ".json").toURL());
			JSONObject jo = jsonArray.getJSONObject(0);
			Purchase purchase = new Purchase();
			// synchronized (purchase) {
			purchase.setId(jo.getLong("id"));
			purchase.setPurchaseItems(getPurchaseItem(purchase.getId()));
			// }
			return purchase;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	private List<PurchaseItem> getPurchaseItem(Long purchaseID) {
		try {
			List<PurchaseItem> purchaseItems = new ArrayList<PurchaseItem>();
			JSONArray jsonArray = parser.getJSONData(App.getInstance()
					.getBaseJSONUrl("purchaseItem/purchaseId/" + purchaseID + ".json").toURL());

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jo = jsonArray.getJSONObject(i);
				PurchaseItem purchaseItem = new PurchaseItem();
				purchaseItem.setId(jo.getLong("id"));
				purchaseItem.setName(jo.getString("name"));
				purchaseItem.setPrice(jo.getDouble("price"));
				purchaseItem.setDiscount(jo.getDouble("discount"));
				purchaseItem.setPurchaseOptions(getPurchaseOption(purchaseItem.getId()));
				purchaseItems.add(purchaseItem);
			}

			return purchaseItems;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

	private List<PurchaseOption> getPurchaseOption(Long purchaseItemID) {
		try {
			List<PurchaseOption> purchaseOptions = new ArrayList<PurchaseOption>();
			JSONArray jsonArray = parser.getJSONData(App.getInstance()
					.getBaseJSONUrl("purchaseOption/purchaseItemId/" + purchaseItemID + ".json").toURL());

			for (int i = 0; i < jsonArray.length(); i++) {
				JSONObject jo = jsonArray.getJSONObject(i);
				PurchaseOption purchaseOption = new PurchaseOption();
				// synchronized (purchaseOption) {
				purchaseOption.setId(jo.getLong("id"));
				purchaseOption.setName(jo.getString("name"));
				purchaseOption.setPrice(jo.getDouble("price"));
				purchaseOptions.add(purchaseOption);
				// }
			}
			return purchaseOptions;
		} catch (MalformedURLException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (URISyntaxException e) {
			e.printStackTrace();
		}
		return null;
	}

}
