package org.dailycode.pawon.activity;

import org.dailycode.pawon.R;
import org.dailycode.pawon.adapter.ListCartAdapter;
import org.dailycode.pawon.entity.Purchase;
import org.dailycode.pawon.entity.PurchaseItem;
import org.dailycode.pawon.helper.ActivityContext;
import org.dailycode.pawon.helper.PawonTask;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.ListView;

public class CartActivity extends PawonActivity implements ActivityContext<CartActivity> {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_cart);
		new CartTask(getContext()).execute();
		Log.d("table ", getPrefTable() + " : " + getPrefServerIP() + " : " + getPrefPort() + " : " + getPrefLanguage());
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.home, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_home:
			startActivity(new Intent(this, MainActivity.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	class CartTask extends PawonTask<PurchaseItem> {

		private Purchase purchase = new Purchase();

		public CartTask(PawonActivity context) {
			super(context);
		}

		@Override
		protected Boolean doInBackground(Object... params) {
			purchase = getJSONService().getPurchaseByRoom(getPrefTable());
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			ListView listView = (ListView) findViewById(R.id.listItemCart);
			listView.setAdapter(new ListCartAdapter(getContext(), R.id.textViewTitle, purchase.getPurchaseItems()));
			super.onPostExecute(result);
		}

	}

	@Override
	public CartActivity getContext() {
		return CartActivity.this;
	}

}
