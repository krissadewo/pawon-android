package org.dailycode.pawon.activity;

import org.dailycode.pawon.preference.SettingPreference;

import android.os.Bundle;

public class SettingActivity extends PawonActivity {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		getFragmentManager().beginTransaction().replace(android.R.id.content, new SettingPreference(this)).commit();
	}

	/**
	 * Override this method with blank code, do not call super implementation
	 * 
	 */
	@Override
	public void onSaveInstanceState(Bundle outState) {

	}
}
