package org.dailycode.pawon.activity;

import java.util.ArrayList;
import java.util.List;

import org.dailycode.pawon.App;
import org.dailycode.pawon.R;
import org.dailycode.pawon.entity.ItemCategory;
import org.dailycode.pawon.service.JSONService;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;

/**
 * Global class for those to maintain global activity state, this class will
 * handle general operation for activity like alerting error/information message
 * and providing base service class to accessing json data.
 * 
 * You should extend this class if you will created basic activity class. The
 * json data helper will through this class.
 * 
 * @author kris
 * 
 */
public abstract class PawonActivity extends Activity {

	private ProgressDialog progressDialog;
	private AlertDialog.Builder alert;
	private SharedPreferences prefs;
	/**
	 * Record current category in one activity
	 */
	private List<ItemCategory> currentCategory = new ArrayList<ItemCategory>();
	/**
	 * Record current position in one activity
	 */
	protected static int currentParentCategoryPosition = -1;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		if (android.os.Build.VERSION.SDK_INT > 9) {
			StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitNetwork().permitDiskReads()
					.permitDiskReads().build();
			StrictMode.setThreadPolicy(policy);
		}
		prefs = PreferenceManager.getDefaultSharedPreferences(this);
		App.getInstance().setHostURL("http://" + getPrefServerIP() + ":" + getPrefPort());

	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.cart, menu);
		return true;
	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_cart:
			startActivity(new Intent(this, CartActivity.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}
	}

	public void messageForNotConnected(Activity activity) {
		alert = new AlertDialog.Builder(activity);
		alert.setTitle("Information");
		alert.setMessage("Can't connect to server, please contact your administrator");
		alert.setPositiveButton(android.R.string.yes, null);
		alert.show();
	}

	public void messageForNotConnected() {
		alert = new AlertDialog.Builder(this);
		alert.setTitle("Information");
		alert.setMessage("Can't connect to server, please contact your administrator");
		alert.setPositiveButton(android.R.string.yes, null);
		alert.show();
	}

	public void messageForDataNotExist(Activity activity) {
		alert = new AlertDialog.Builder(activity);
		alert.setTitle("Information");
		alert.setMessage("Data not exist...");
		alert.setPositiveButton(android.R.string.yes, null);
		alert.show();
	}

	public void messageForOrderSuccess() {
		alert = new AlertDialog.Builder(this);
		alert.setTitle("Information");
		alert.setMessage("Your order has been record, check your order on MY CART ...");
		alert.setPositiveButton(android.R.string.yes, null);
		alert.show();
	}

	public void messageForOrderFailed() {
		alert = new AlertDialog.Builder(this);
		alert.setTitle("Information");
		alert.setMessage("We apologize, your order can't be process right now, contact your support for this reason");
		alert.setPositiveButton(android.R.string.yes, null);
		alert.show();
	}

	public JSONService getJSONService() {
		return JSONService.getInstance();
	}

	public ProgressDialog showProgressLoadingDialog() {
		return progressDialog = ProgressDialog.show(this, "Please Wait", "Loading...");
	}

	public void closeProgressLoadingDialog() {
		progressDialog.dismiss();
	}

	public String getPrefServerIP() {
		// return getSharedPreferences(App.SERVER_ADDRESS,
		// MODE_PRIVATE).getString(App.SERVER_ADDRESS, "127.0.0.1");
		return prefs.getString(App.PREF_SERVER, "127.0.0.1");
	}

	@Deprecated
	public void setServerAddress(String serverAddress) {
		SharedPreferences.Editor sharedPreferences = getSharedPreferences(App.PREF_SERVER, MODE_PRIVATE).edit();
		sharedPreferences.putString(App.PREF_SERVER, serverAddress).commit();

	}

	public String getPrefPort() {
		return prefs.getString(App.PREF_PORT, "9666");
	}

	@Deprecated
	public void setPortAddress(String portAddress) {
		SharedPreferences.Editor sharedPreferences = getSharedPreferences(App.PREF_PORT, MODE_PRIVATE).edit();
		sharedPreferences.putString(App.PREF_PORT, portAddress).commit();
	}

	/**
	 * Get table name from preference
	 * 
	 * @return
	 */
	public String getPrefTable() {
		// return getSharedPreferences(App.TABLE_NAME,
		// MODE_PRIVATE).getString(App.TABLE_NAME, null);
		return prefs.getString(App.PREF_TABLE, null);
	}

	/**
	 * Set table name on preference
	 * 
	 * @param tableName
	 */
	@Deprecated
	public void setTableName(String tableName) {
		SharedPreferences.Editor sharedPreferences = getSharedPreferences(App.PREF_TABLE, MODE_PRIVATE).edit();
		sharedPreferences.putString(App.PREF_TABLE, tableName).commit();
	}

	public Long getPrefLanguage() {
		// return getSharedPreferences(App.LANGUAGE_ID,
		// MODE_PRIVATE).getLong(App.LANGUAGE_ID, -1l);
		return Long.valueOf(prefs.getString(App.PREF_LANGUAGE, null));
	}

	public Boolean getPrefServerStatus() {
		return prefs.getBoolean(App.PREF_STATUS, false);
	}

	@Deprecated
	public void setLanguageID(Long languangeID) {
		SharedPreferences.Editor sharedPreferences = getSharedPreferences(App.PREF_LANGUAGE, MODE_PRIVATE).edit();
		sharedPreferences.putLong(App.PREF_LANGUAGE, languangeID).commit();
	}

	public List<ItemCategory> getCurrentCategory() {
		return currentCategory;
	}

}
