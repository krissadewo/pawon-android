package org.dailycode.pawon.activity;

import java.util.ArrayList;
import java.util.List;

import org.dailycode.pawon.App;
import org.dailycode.pawon.R;
import org.dailycode.pawon.entity.Item;
import org.dailycode.pawon.entity.ItemOption;
import org.dailycode.pawon.fragment.DialogOptionFragment;
import org.dailycode.pawon.helper.ActivityContext;
import org.dailycode.pawon.helper.ImageLoader;
import org.dailycode.pawon.helper.PawonTask;

import android.app.DialogFragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.text.Html;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class ItemDetailActivity extends PawonActivity implements ActivityContext<ItemDetailActivity> {

	private List<ItemOption> allItemOptions = new ArrayList<ItemOption>();
	private Button btnOption;
	private Button btnOrder;
	private Item item;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item_detail);
		new ItemTask(getContext()).execute();

		btnOption = (Button) findViewById(R.id.buttonOption);
		btnOrder = (Button) findViewById(R.id.buttonOrder);

		btnOption.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				FragmentManager fragmentManager = getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
				fragmentTransaction.addToBackStack(null);
				DialogFragment dialogFragment = new DialogOptionFragment(allItemOptions);
				dialogFragment.show(fragmentTransaction, "");
			}
		});

		btnOrder.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				new OrderTask(getContext()).execute();
			}
		});

		App.getInstance().deleteCheckedOptions();

	}

	class OrderTask extends PawonTask<Boolean> {

		public OrderTask(PawonActivity context) {
			super(context);
		}

		protected Boolean doInBackground(Object... params) {
			StringBuffer checkedOption = new StringBuffer();
			List<ItemOption> itemOptions = App.getInstance().getCurrentCheckedOptions();
			if (!itemOptions.isEmpty()) {
				for (ItemOption option : App.getInstance().getCurrentCheckedOptions()) {
					checkedOption.append(option.getId());
					checkedOption.append(",");
				}
				
				if (checkedOption.length() > 0) {
					checkedOption.deleteCharAt(checkedOption.length() - 1);
				}
			} else {
				/**
				 * If no exist checked item option, we should set the default
				 * value for this parameter, the server need check this value
				 */
				checkedOption.append("-1");
			}

			return getJSONService().orderPurchase(item.getId(), checkedOption.toString(), getPrefTable(),
					getPrefLanguage());
		}

		protected void onPostExecute(Boolean result) {
			super.onPostExecute(result);
			if (result) {
				messageForOrderSuccess();
			} else {
				messageForOrderFailed();
			}
			App.getInstance().deleteCheckedOptions();
		}

	}

	class ItemTask extends PawonTask<Item> {

		private List<ItemOption> itemOptions = new ArrayList<ItemOption>();

		public ItemTask(PawonActivity activity) {
			super(activity);
		}

		@Override
		protected Boolean doInBackground(Object... params) {
			item = getJSONService().getItemByID(getIntent().getLongExtra("itemID", -1), getPrefLanguage());
			itemOptions = getJSONService().getItemOptionByItem(item.getId(), getPrefLanguage());
			for (ItemOption itemOption : itemOptions) {
				List<ItemOption> childOptions = getJSONService().getItemOptionByParent(itemOption.getId(),
						getPrefLanguage());
				allItemOptions.add(itemOption);

				if (childOptions != null) {
					for (ItemOption childOption : childOptions) {
						allItemOptions.add(childOption);
					}
				}
			}

			return true;

		}

		protected void onPostExecute(Boolean result) {
			if (!itemOptions.isEmpty()) {
				btnOption.setVisibility(Button.VISIBLE);
			}

			TextView textTitle = (TextView) findViewById(R.id.textTitle);
			textTitle.setText(item.getLocalizedInformation().getTitle());

			TextView textDescription = (TextView) findViewById(R.id.textDescription);
			textDescription.setText(Html.fromHtml(item.getLocalizedInformation().getDescription()));

			TextView textPrice = (TextView) findViewById(R.id.textPrice);
			textPrice.setText("Price : " + App.convertNumToIDR(item.getPrice()));

			TextView textDiscount = (TextView) findViewById(R.id.textDiscount);
			textDiscount.setText("Discount : " + item.getDiscount() + "%");

			ImageView imageItemDetail = (ImageView) findViewById(R.id.imageItemDetail);

			String imageUrl = App.getInstance().getHostURL() + "/media/" + item.getImage().replace(" ", "%20");
			ImageLoader imageLoader = new ImageLoader(getContext());
			imageLoader.setImageHeight(360);
			imageLoader.setImageWidth(360);
			imageLoader.DisplayImage(imageUrl, R.drawable.gradient_bg, imageItemDetail);
			super.onPostExecute(result);
		}
	}

	@Override
	public ItemDetailActivity getContext() {
		return ItemDetailActivity.this;
	}

}
