package org.dailycode.pawon.activity;

import java.util.ArrayList;
import java.util.List;

import org.dailycode.pawon.R;
import org.dailycode.pawon.adapter.GridItemAdapter;
import org.dailycode.pawon.entity.Item;
import org.dailycode.pawon.helper.ActivityContext;
import org.dailycode.pawon.helper.PawonTask;

import android.os.Bundle;
import android.widget.GridView;

public class ItemActivity extends PawonActivity implements ActivityContext<ItemActivity> {

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_item);		
		new ItemTask(getContext()).execute();
	}

	class ItemTask extends PawonTask<Item> {

		private List<Item> items = new ArrayList<Item>();

		public ItemTask(PawonActivity activity) {
			super(activity);
		}

		@Override
		protected Boolean doInBackground(Object... params) {
			items = getJSONService().getItems(getIntent().getLongExtra("categoryID", -1), getPrefLanguage());
			return null;
		}

		protected void onPostExecute(Boolean result) {
			GridView gridView = (GridView) findViewById(R.id.gridItem);
			gridView.setAdapter(new GridItemAdapter(getContext(), R.id.textViewTitle, items));
			super.onPostExecute(result);
		}

	}

	@Override
	public ItemActivity getContext() {
		return ItemActivity.this;
	}

}
