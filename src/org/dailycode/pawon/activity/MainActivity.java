package org.dailycode.pawon.activity;

import java.util.ArrayList;
import java.util.List;

import org.dailycode.pawon.R;
import org.dailycode.pawon.adapter.MenuCategoryAdapter;
import org.dailycode.pawon.entity.ItemCategory;
import org.dailycode.pawon.helper.ActivityContext;
import org.dailycode.pawon.helper.PawonTask;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;

public class MainActivity extends PawonActivity implements ActivityContext<MainActivity> {

	private GridView gridView;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		new CategoryTask(getContext()).execute();
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

	public void onBackPressed(int keyCode, KeyEvent event) {

	}

	@Override
	public void onBackPressed() {
		if (!getCurrentCategory().isEmpty()) {
			List<ItemCategory> currentCategories = new ArrayList<ItemCategory>();
			List<ItemCategory> categories = new ArrayList<ItemCategory>();

			if (currentParentCategoryPosition == 0) {
				currentCategories = getJSONService().getCategory(getPrefLanguage());
				for (ItemCategory category : currentCategories) {
					if (category.getParentId() == 0) {
						categories.add(category);
					}
				}
			} else {
				currentCategories = getJSONService().getChildCategory(
						getCurrentCategory().get(currentParentCategoryPosition).getParentId(), getPrefLanguage());
				for (ItemCategory category : currentCategories) {
					categories.add(category);
				}

			}

			createListMenuData(categories);
			getCurrentCategory().remove(currentParentCategoryPosition);
			currentParentCategoryPosition = currentParentCategoryPosition - 1;
		} else {
			new AlertDialog.Builder(this).setTitle("Information").setMessage("Do you really want to exit ?")
					.setIcon(android.R.drawable.ic_dialog_alert)
					.setPositiveButton(android.R.string.yes, new DialogInterface.OnClickListener() {

						public void onClick(DialogInterface dialog, int whichButton) {
							getContext().finish();
						}
					}).setNegativeButton(android.R.string.no, null).show();
		}

	}

	public boolean onOptionsItemSelected(MenuItem item) {
		switch (item.getItemId()) {
		case R.id.action_cart:
			startActivity(new Intent(this, CartActivity.class));
			return true;
		case R.id.action_refresh:
			recreate();
			return true;
		case R.id.action_settings:
			startActivity(new Intent(this, SettingActivity.class));
			return true;
		default:
			return super.onOptionsItemSelected(item);
		}

	}

	class CategoryTask extends PawonTask<ItemCategory> {

		private List<ItemCategory> categories = new ArrayList<ItemCategory>();

		public CategoryTask(PawonActivity activity) {
			super(activity);
		}

		@Override
		protected Boolean doInBackground(Object... params) {
			categories = getJSONService().getCategory(getPrefLanguage());

			return null;
		}

		protected void onPostExecute(Boolean result) {
			gridView = (GridView) findViewById(R.id.gridCategory);

			final List<ItemCategory> parentCategories = new ArrayList<ItemCategory>();
			if (categories != null) {
				for (ItemCategory category : categories) {
					if (category.getParentId() == 0) {
						parentCategories.add(category);
					}
				}
				createListMenuData(parentCategories);
			}
			super.onPostExecute(result);

		}

	}

	private List<ItemCategory> createListMenuData(final List<ItemCategory> categories) {
		List<ItemCategory> childCategories = new ArrayList<ItemCategory>();
		gridView.setAdapter(new MenuCategoryAdapter(getContext(), R.id.textViewTitle, categories));
		if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_LANDSCAPE) {
			if (categories.size() == 1) {
				gridView.setNumColumns(1);
			} else {
				gridView.setNumColumns(GridView.AUTO_FIT);
			}
		} else {
			gridView.setNumColumns(1);
		}

		gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {

			@Override
			public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

				if (getJSONService().isCategoryHasChild(categories.get(position).getId())) {
					createCategories(categories.get(position).getId());
					getCurrentCategory().add(categories.get(position));
					currentParentCategoryPosition = currentParentCategoryPosition + 1;
				} else {
					generatedItem(categories.get(position).getId());
				}
			}

			/**
			 * Recursive pattern, call parent method from parent class to
			 * achieve unlimited category on menu
			 * 
			 * @param id
			 * @return categories
			 */
			private List<ItemCategory> createCategories(Long id) {
				return createListMenuData(getJSONService().getChildCategory(id, getPrefLanguage()));
			}

		});

		return childCategories;
	}

	public void generatedItem(Long categoryID) {
		Intent intent = new Intent(MainActivity.this, ItemActivity.class);
		intent.putExtra("categoryID", categoryID);
		intent.putExtra("imageRepo", "http://" + getPrefServerIP() + ":" + getPrefPort() + "/image");
		startActivity(intent);
	}

	@Override
	public MainActivity getContext() {
		return MainActivity.this;
	}

}
