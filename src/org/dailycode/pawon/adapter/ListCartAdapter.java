package org.dailycode.pawon.adapter;

import java.util.List;

import org.dailycode.pawon.App;
import org.dailycode.pawon.R;
import org.dailycode.pawon.entity.ItemOption;
import org.dailycode.pawon.entity.PurchaseItem;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class ListCartAdapter extends ArrayAdapter<PurchaseItem> {

	private static LayoutInflater inflater = null;
	private List<ItemOption> checkedtemOptions;
	private ViewHolder holder;

	public ListCartAdapter(Context context, int textViewResourceId, List<PurchaseItem> objects) {
		super(context, textViewResourceId, objects);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		checkedtemOptions = App.getInstance().getCurrentCheckedOptions();
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		if (convertView == null) {
			convertView = inflater.inflate(R.layout.list_option, null);
			holder = new ViewHolder();
			holder.checkBox = (CheckBox) convertView.findViewById(R.id.checkBoxOption);
			holder.textViewOption = (TextView) convertView.findViewById(R.id.textViewTitle);
			holder.textViewPrice = (TextView) convertView.findViewById(R.id.textViewPrice);
			convertView.setTag(holder);
		} else {
			holder = (ViewHolder) convertView.getTag();
		}

		final PurchaseItem purchaseItem = (PurchaseItem) this.getItem(position);

		String title = purchaseItem.getName();
		String price = App.convertNumToIDR(purchaseItem.getPrice());

		holder.textViewOption.setText(title);
		holder.textViewPrice.setText(price);
		holder.checkBox.setText(title);
		holder.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

			@Override
			public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {

			}
		});

		return convertView;
	}

	public List<ItemOption> getCheckedtemOptions() {
		return checkedtemOptions;
	}

	static class ViewHolder {
		private CheckBox checkBox;
		private TextView textViewOption;
		private TextView textViewPrice;

	}

}
