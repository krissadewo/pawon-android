package org.dailycode.pawon.adapter;

import java.util.List;

import org.dailycode.pawon.App;
import org.dailycode.pawon.R;
import org.dailycode.pawon.activity.ItemDetailActivity;
import org.dailycode.pawon.entity.Item;
import org.dailycode.pawon.helper.ImageLoader;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

public class GridItemAdapter extends ArrayAdapter<Item> {

	private List<Item> data;
	private static LayoutInflater inflater = null;
	private Context context;

	public GridItemAdapter(Context context, int textViewResourceId, List<Item> objects) {
		super(context, textViewResourceId, objects);
		this.context = context;
		this.data = objects;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return data.size();
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null) {
			vi = inflater.inflate(R.layout.grid_item, null);

			TextView title = (TextView) vi.findViewById(R.id.textViewTitle);
			final ImageView imageView = (ImageView) vi.findViewById(R.id.gridItemImage);

			imageView.setOnClickListener(new View.OnClickListener() {

				@Override
				public void onClick(View v) {
					imageView.setAlpha(0.5f);
					Intent intent = new Intent(context, ItemDetailActivity.class);
					intent.putExtra("itemID", data.get(position).getId());
					context.startActivity(intent);
				}
			});

			String imageUrl = App.getInstance().getHostURL() + "/media/"
					+ data.get(position).getImage().replace(" ", "%20");
			imageView.setImageURI(Uri.parse(imageUrl));
			ImageLoader imageLoader = new ImageLoader(context);
			imageLoader.DisplayImage(imageUrl, R.drawable.gradient_bg, imageView);
			String text = data.get(position).getName();
			title.setText(text);
		}

		return vi;
	}

}
