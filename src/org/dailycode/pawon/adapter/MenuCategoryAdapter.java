package org.dailycode.pawon.adapter;

import java.util.List;

import org.dailycode.pawon.R;
import org.dailycode.pawon.entity.ItemCategory;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

public class MenuCategoryAdapter extends ArrayAdapter<ItemCategory> {

	private List<ItemCategory> data;
	private static LayoutInflater inflater = null;

	public MenuCategoryAdapter(Context context, int textViewResourceId, List<ItemCategory> objects) {
		super(context, textViewResourceId, objects);
		this.data = objects;
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}

	public int getCount() {
		return data.size();
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null)
			vi = inflater.inflate(R.layout.grid_category, null);

		TextView title = (TextView) vi.findViewById(R.id.textViewTitle);
		title.setText(data.get(position).getLocalizedInformation().getTitle());

		return vi;
	}

}
