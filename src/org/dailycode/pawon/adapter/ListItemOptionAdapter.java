package org.dailycode.pawon.adapter;

import java.util.Iterator;
import java.util.List;

import org.dailycode.pawon.App;
import org.dailycode.pawon.R;
import org.dailycode.pawon.entity.ItemOption;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;

public class ListItemOptionAdapter extends ArrayAdapter<ItemOption> {

	private static LayoutInflater inflater = null;
	private List<ItemOption> checkedtemOptions;

	public ListItemOptionAdapter(Context context, int textViewResourceId, List<ItemOption> objects) {
		super(context, textViewResourceId, objects);
		inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
		checkedtemOptions = App.getInstance().getCurrentCheckedOptions();
	}

	public long getItemId(int position) {
		return position;
	}

	public View getView(final int position, View convertView, ViewGroup parent) {
		View vi = convertView;
		if (convertView == null) {
			Log.d("convert", "asasas");
			vi = inflater.inflate(R.layout.list_option, null);
			final ItemOption itemOption = (ItemOption) this.getItem(position);

			String title = itemOption.getLocalizedInformation().getTitle();
			String price = App.convertNumToIDR(itemOption.getPrice());
			
			CheckBox checkBox = (CheckBox) vi.findViewById(R.id.checkBoxOption);
			TextView textViewTitle = (TextView) vi.findViewById(R.id.textViewTitle);
			TextView textViewPrice = (TextView)vi.findViewById(R.id.textViewPrice);
			
			if (itemOption.getParentId() == 0) {
				textViewTitle.setText(title);		
				checkBox.setVisibility(View.INVISIBLE);
			}else{
				textViewPrice.setText(price);
			}

			for (ItemOption option : getCheckedtemOptions()) {
				if (option.getName().equalsIgnoreCase(itemOption.getName())) {
					checkBox.setChecked(true);
				}
			}
			
			checkBox.setText(title);
			checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

				@Override
				public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
					if (isChecked) {
						getCheckedtemOptions().add(itemOption);
					} else {
						/**
						 * We should consider about concurent modification
						 * exception when removing object from array list, so we
						 * need using iterator for this case,
						 */
						Iterator<ItemOption> iterator = getCheckedtemOptions().iterator();
						while (iterator.hasNext()) {
							ItemOption item = (ItemOption) iterator.next();
							if (item.getName().equalsIgnoreCase(itemOption.getName())) {
								iterator.remove();
							}
						}
					}
				}
			});

		}

		return vi;
	}

	public List<ItemOption> getCheckedtemOptions() {
		return checkedtemOptions;
	}

	/** Holds child views for one row. */
	@SuppressWarnings("unused")
	private static class SelectViewHolder {
		private CheckBox checkBox;
		private TextView textView;

	}

}
