/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.entity;

/**
 *
 * @author kris
 */
public enum LanguageCode {

    ENGLISH("en_UK", "lang.eng"),
    INDONESIAN("id_ID", "lang.ind"),
    SPANISH("es_SP", "lang.spa"),
    FRENCH("fr_FR", "lang.fre"),
    RUSSIAN("ru_RU", "lang.rus"),
    KOREAN("ko_KO", "lang.kor"),
    CHINESE("ch_CH", "lang.chi"),
    JAPANESE("ja_JP", "lang.jpn"),
    THAI("th_TH", "lang.tha"),
    GERMAN("de_DE", "lang.ger");
    private String localeCode;
    private String i18nKey;

    LanguageCode(String pLocaleCode, String pI18n) {
        this.localeCode = pLocaleCode;
        this.i18nKey = pI18n;
    }

    public String getI18nKey() {
        return this.i18nKey;
    }

    public String getLocaleCode() {
        return localeCode;
    }

    public void setLocaleCode(String localeCode) {
        this.localeCode = localeCode;
    }

    public void setI18nKey(String i18nKey) {
        this.i18nKey = i18nKey;
    }
}
