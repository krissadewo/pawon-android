/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author kris
 * @created Mar 4, 2013
 */
public class ItemOption implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 2313820990411197032L;
	private Long id;
	private boolean active = true;
	private String name;
	private Long parentId;
	private Integer orderNumber;
	private Section section;
	private Double price;
	private LocalizedInformation localizedInformation;
	private List<LocalizedInformation> localizedInformations = new ArrayList<LocalizedInformation>();
	private boolean checked;

	public ItemOption() {
		super();
		// TODO Auto-generated constructor stub
	}

	public ItemOption(Long id, String name, Long parentId, Double price, LocalizedInformation localizedInformation) {
		this.id = id;
		this.name = name;
		this.parentId = parentId;
		this.price = price;
		this.localizedInformation = localizedInformation;
	}

	public boolean isChecked() {
		return checked;
	}

	public void setChecked(boolean checked) {
		this.checked = checked;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	public Integer getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Integer orderNumber) {
		this.orderNumber = orderNumber;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public List<LocalizedInformation> getLocalizedInformations() {
		return localizedInformations;
	}

	public void setLocalizedInformations(List<LocalizedInformation> localizedInformations) {
		this.localizedInformations = localizedInformations;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public LocalizedInformation getLocalizedInformation() {
		return localizedInformation;
	}

	public void setLocalizedInformation(LocalizedInformation localizedInformation) {
		this.localizedInformation = localizedInformation;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (active ? 1231 : 1237);
		result = prime * result + (checked ? 1231 : 1237);
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((localizedInformation == null) ? 0 : localizedInformation.hashCode());
		result = prime * result + ((localizedInformations == null) ? 0 : localizedInformations.hashCode());
		result = prime * result + ((name == null) ? 0 : name.hashCode());
		result = prime * result + ((orderNumber == null) ? 0 : orderNumber.hashCode());
		result = prime * result + ((parentId == null) ? 0 : parentId.hashCode());
		result = prime * result + ((price == null) ? 0 : price.hashCode());
		result = prime * result + ((section == null) ? 0 : section.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		ItemOption other = (ItemOption) obj;
		if (active != other.active)
			return false;
		if (checked != other.checked)
			return false;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (localizedInformation == null) {
			if (other.localizedInformation != null)
				return false;
		} else if (!localizedInformation.equals(other.localizedInformation))
			return false;
		if (localizedInformations == null) {
			if (other.localizedInformations != null)
				return false;
		} else if (!localizedInformations.equals(other.localizedInformations))
			return false;
		if (name == null) {
			if (other.name != null)
				return false;
		} else if (!name.equals(other.name))
			return false;
		if (orderNumber == null) {
			if (other.orderNumber != null)
				return false;
		} else if (!orderNumber.equals(other.orderNumber))
			return false;
		if (parentId == null) {
			if (other.parentId != null)
				return false;
		} else if (!parentId.equals(other.parentId))
			return false;
		if (price == null) {
			if (other.price != null)
				return false;
		} else if (!price.equals(other.price))
			return false;
		if (section != other.section)
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "ItemOption [id=" + id + ", active=" + active + ", name=" + name + ", parentId=" + parentId
				+ ", section=" + section + ", price=" + price + ", checked=" + checked + "]";
	}

}
