/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.entity;

import java.util.Arrays;

/**
 * 
 * @author Kris Sadewo
 * @date Feb 8, 2013
 */
public class ItemCategory extends ItemObject {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3844207854121701008L;
	private Long parentId = 0l;

	public ItemCategory() {
	}

	public ItemCategory(Long id, String name) {
		super(id, name);
	}

	public Long getParentId() {
		return parentId;
	}

	public void setParentId(Long parentId) {
		this.parentId = parentId;
	}

	@Override
	public String toString() {
		return "ItemCategory [parentId=" + parentId + ", getId()=" + getId() + ", getCode()=" + getCode()
				+ ", getName()=" + getName() + ", getImage()=" + getImage() + ", getLogo()=" + getLogo()
				+ ", getImageData()=" + Arrays.toString(getImageData()) + ", getLogoData()="
				+ Arrays.toString(getLogoData()) + ", isActive()=" + isActive() + ", getSection()=" + getSection()
				+ ", getOrderNumber()=" + getOrderNumber() + ", getLocalizedInformations()="
				+ getLocalizedInformations() + ", getSource()=" + getSource() + ", getLocalizedInformation()="
				+ getLocalizedInformation() + ", isStatusDelete()=" + isStatusDelete() + ", getClass()=" + getClass()
				+ ", hashCode()=" + hashCode() + ", toString()=" + super.toString() + "]";
	}

}
