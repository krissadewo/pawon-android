package org.dailycode.pawon.entity;

import java.util.ArrayList;
import java.util.List;

/**
 * The purchase item represents all things customers can purchase, except for
 * the movie itself
 */
public class Item extends ItemObject {

	private static final long serialVersionUID = -1613397147107262538L;
	private Double discount = 0d;
	private Double price = 0d;
	private ItemCategory itemCategory;
	private List<ItemOption> itemOptions = new ArrayList<ItemOption>();

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public ItemCategory getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(ItemCategory itemCategory) {
		this.itemCategory = itemCategory;
	}

	public List<ItemOption> getItemOptions() {
		return itemOptions;
	}

	public void setItemOptions(List<ItemOption> itemOptions) {
		this.itemOptions = itemOptions;
	}

	@Override
	public String toString() {
		return "Item [discount=" + discount + ", price=" + price + ", itemCategory=" + itemCategory + ", itemOptions="
				+ itemOptions + "]";
	}

}
