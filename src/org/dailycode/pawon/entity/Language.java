/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.entity;

import java.io.Serializable;


/**
 *
 * @author kris
 */
public class Language implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -3327707268794380233L;
	private Long id;
    private String name;
    private String code;
    private boolean defaultLanguage = false;
    private boolean active = true;
    private String image;
    private boolean statusDelete;
    private byte[] imageData;
    private LanguageCode languageCode;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isDefaultLanguage() {
        return defaultLanguage;
    }

    public void setDefaultLanguage(boolean defaultLanguage) {
        this.defaultLanguage = defaultLanguage;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public boolean isStatusDelete() {
        return statusDelete;
    }

    public void setStatusDelete(boolean statusDelete) {
        this.statusDelete = statusDelete;
    }

    public byte[] getImageData() {
        return imageData;
    }

    public void setImageData(byte[] imageData) {
        this.imageData = imageData;
    }

    public LanguageCode getLanguageCode() {
        return languageCode;
    }

    public void setLanguageCode(LanguageCode languageCode) {
        this.languageCode = languageCode;
    }

    @Override
    public String toString() {
        return "Language{" + "id=" + id + ", name=" + name + ", code=" + code + ", defaultLanguage=" + defaultLanguage + ", active=" + active + ", image=" + image + ", statusDelete=" + statusDelete + ", imageData=" + imageData + '}';
    }
}
