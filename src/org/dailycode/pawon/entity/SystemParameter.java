/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.entity;

import java.io.Serializable;

/**
 *
 * @author kris
 */
public class SystemParameter implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = -8046529738760203135L;
	private Long id;
    private String imageURL;
    private String imageSource;
    private String printerName;
    private Integer printCopy;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getImageURL() {
        return imageURL;
    }

    public void setImageURL(String imageURL) {
        this.imageURL = imageURL;
    }

    public String getImageSource() {
        return imageSource;
    }

    public void setImageSource(String imageSource) {
        this.imageSource = imageSource;
    }

    public String getPrinterName() {
        return printerName;
    }

    public void setPrinterName(String printerName) {
        this.printerName = printerName;
    }

    public Integer getPrintCopy() {
        return printCopy;
    }

    public void setPrintCopy(Integer printCopy) {
        this.printCopy = printCopy;
    }

    @Override
    public String toString() {
        return "SystemParameter{" + "id=" + id + ", imageURL=" + imageURL + ", imageSource=" + imageSource + ", printerName=" + printerName + ", printCopy=" + printCopy + '}';
    }
}
