/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.entity;

/**
 *
 * @author kris
 * @created Mar 21, 2013
 */
public enum PurchaseStatus {

    ORDERED,
    PROCESS,
    COMPLETE;

    public static PurchaseStatus getPurchasStatus(String pName) {
        if (pName == null) {
            return null;
        }
        for (PurchaseStatus purchaseStatus : PurchaseStatus.values()) {
            if (pName.toLowerCase().contains(purchaseStatus.name().toLowerCase())) {
                return purchaseStatus;
            }
        }
        return null;
    }
}
