/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author kris
 * @created Mar 18, 2013
 */
public class PurchaseItem implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -3122074961091612932L;
	private Long id;
	private String name;
	private Double price;
	private Double discount;
	private Purchase purchase;
	private Item item;
	private List<PurchaseOption> purchaseOptions = new ArrayList<PurchaseOption>();

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public Double getPrice() {
		return price;
	}

	public void setPrice(Double price) {
		this.price = price;
	}

	public Double getDiscount() {
		return discount;
	}

	public void setDiscount(Double discount) {
		this.discount = discount;
	}

	public Purchase getPurchase() {
		return purchase;
	}

	public void setPurchase(Purchase purchase) {
		this.purchase = purchase;
	}

	public List<PurchaseOption> getPurchaseOptions() {
		return purchaseOptions;
	}

	public void setPurchaseOptions(List<PurchaseOption> purchaseOptions) {
		this.purchaseOptions = purchaseOptions;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	@Override
	public String toString() {
		return "PurchaseItem{" + "id=" + id + ", name=" + name + ", price=" + price + ", discount=" + discount
				+ ", purchase=" + purchase + ", item=" + item + ", purchaseOptions=" + purchaseOptions + '}';
	}
}
