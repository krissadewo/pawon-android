/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.entity;

import java.io.Serializable;
import java.util.List;

/**
 *
 * @author kris
 * @created Mar 18, 2013
 */
public class Room implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 5859549792155376789L;
	private Long id;
    private String name;
    private String description;
    private Purchase purchase;
    private List<Purchase> purchases;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Purchase getPurchase() {
        return purchase;
    }

    public void setPurchase(Purchase purchase) {
        this.purchase = purchase;
    }

    public List<Purchase> getPurchases() {
        return purchases;
    }

    public void setPurchases(List<Purchase> purchases) {
        this.purchases = purchases;
    }

    @Override
    public String toString() {
        return "Room{" + "id=" + id + ", name=" + name + ", description=" + description + ", purchase=" + purchase + ", purchases=" + purchases + '}';
    }
}
