package org.dailycode.pawon.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


/**
 * 
 * @author kris
 */
public class ItemObject implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -1444890170742689902L;
	private Long id;
	private String code;
	private String name;
	private String image;
	private String logo;
	private byte[] imageData;
	private byte[] logoData;
	private boolean active = true;
	private Section section;
	private int orderNumber;
	private List<LocalizedInformation> localizedInformations = new ArrayList<LocalizedInformation>();
	private LocalizedInformation localizedInformation;
	private String source;
	private boolean statusDelete;

	public ItemObject(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public ItemObject() {
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public String getLogo() {
		return logo;
	}

	public void setLogo(String logo) {
		this.logo = logo;
	}

	public byte[] getImageData() {
		return imageData;
	}

	public void setImageData(byte[] imageData) {
		this.imageData = imageData;
	}

	public byte[] getLogoData() {
		return logoData;
	}

	public void setLogoData(byte[] logoData) {
		this.logoData = logoData;
	}

	public boolean isActive() {
		return active;
	}

	public void setActive(boolean active) {
		this.active = active;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public int getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(int orderNumber) {
		this.orderNumber = orderNumber;
	}

	public List<LocalizedInformation> getLocalizedInformations() {
		return localizedInformations;
	}

	public void setLocalizedInformations(List<LocalizedInformation> localizedInformations) {
		this.localizedInformations = localizedInformations;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public LocalizedInformation getLocalizedInformation() {
		return localizedInformation;
	}

	public void setLocalizedInformation(LocalizedInformation localizedInformation) {
		this.localizedInformation = localizedInformation;
	}

	public boolean isStatusDelete() {
		return statusDelete;
	}

	public void setStatusDelete(boolean statusDelete) {
		this.statusDelete = statusDelete;
	}

}
