package org.dailycode.pawon.entity;

import java.io.Serializable;

/**
 * The {@link _LocalizedInformation} represents the language specific
 * information of {@link PurchaseItem}<br>
 * All localized information like name and description are stored here <br>
 * All central information like price, image, section, category and sorting
 * information are stored central in {@link PurchaseItem}
 */
public class LocalizedInformation implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 520593013774598477L;
	private Long id;
	private String title;
	private String description;
	private Item item;
	private ItemCategory itemCategory;
	private Language language;
	private ItemOption itemOption;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Language getLanguage() {
		return language;
	}

	public void setLanguage(Language language) {
		this.language = language;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public Item getItem() {
		return item;
	}

	public void setItem(Item item) {
		this.item = item;
	}

	public ItemCategory getItemCategory() {
		return itemCategory;
	}

	public void setItemCategory(ItemCategory itemCategory) {
		this.itemCategory = itemCategory;
	}

	public ItemOption getItemOption() {
		return itemOption;
	}

	public void setItemOption(ItemOption itemOption) {
		this.itemOption = itemOption;
	}

	@Override
	public String toString() {
		return "LocalizedInformation{" + "id=" + id + ", title=" + title + ", description=" + description + ", item="
				+ item + ", itemCategory=" + itemCategory + ", language=" + language + '}';
	}
}
