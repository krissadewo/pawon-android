/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.entity;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


/**
 * 
 * @author kris
 * @created Mar 18, 2013
 */
public class Purchase implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = -8614869276186346938L;
	private Long id;
	private Room room;
	private PurchaseStatus purchaseStatus;
	private Date datePurchase;
	private PurchaseItem purchaseItem;
	private List<PurchaseItem> purchaseItems = new ArrayList<PurchaseItem>();
	private Double totalPrice;
	private Double totalDiscount;
	private Long orderNumber;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Room getRoom() {
		return room;
	}

	public void setRoom(Room room) {
		this.room = room;
	}

	public List<PurchaseItem> getPurchaseItems() {
		return purchaseItems;
	}

	public void setPurchaseItems(List<PurchaseItem> purchaseItems) {
		this.purchaseItems = purchaseItems;
	}

	public PurchaseStatus getPurchaseStatus() {
		return purchaseStatus;
	}

	public void setPurchaseStatus(PurchaseStatus purchaseStatus) {
		this.purchaseStatus = purchaseStatus;
	}

	public Date getDatePurchase() {
		return datePurchase;
	}

	public void setDatePurchase(Date datePurchase) {
		this.datePurchase = datePurchase;
	}

	public PurchaseItem getPurchaseItem() {
		return purchaseItem;
	}

	public void setPurchaseItem(PurchaseItem purchaseItem) {
		this.purchaseItem = purchaseItem;
	}

	public Double getTotalPrice() {
		return totalPrice;
	}

	public void setTotalPrice(Double totalPrice) {
		this.totalPrice = totalPrice;
	}

	public Double getTotalDiscount() {
		return totalDiscount;
	}

	public void setTotalDiscount(Double totalDiscount) {
		this.totalDiscount = totalDiscount;
	}

	public Long getOrderNumber() {
		return orderNumber;
	}

	public void setOrderNumber(Long orderNumber) {
		this.orderNumber = orderNumber;
	}

	@Override
	public String toString() {
		return "Purchase{" + "id=" + id + ", room=" + room + ", purchaseStatus=" + purchaseStatus + ", datePurchase="
				+ datePurchase + ", purchaseItem=" + purchaseItem + ", purchaseItems=" + purchaseItems
				+ ", totalPrice=" + totalPrice + ", totalDiscount=" + totalDiscount + '}';
	}
}
