/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package org.dailycode.pawon.entity;

import java.io.Serializable;

/**
 *
 * @author kris
 * @created Mar 18, 2013
 */
public class PurchaseOption implements Serializable {

    /**
	 * 
	 */
	private static final long serialVersionUID = 3640911166315938385L;
	private Long id;
    private String name;
    private Double price;
    private ItemOption itemOption;
    private PurchaseItem purchaseItem;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Double getPrice() {
        return price;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public ItemOption getItemOption() {
        return itemOption;
    }

    public void setItemOption(ItemOption itemOption) {
        this.itemOption = itemOption;
    }

    public PurchaseItem getPurchaseItem() {
        return purchaseItem;
    }

    public void setPurchaseItem(PurchaseItem purchaseItem) {
        this.purchaseItem = purchaseItem;
    }

    @Override
    public String toString() {
        return "PurchaseOption{" + "id=" + id + ", name=" + name + ", price=" + price + ", itemOption=" + itemOption + ", purchaseItem=" + purchaseItem + '}';
    }
}
