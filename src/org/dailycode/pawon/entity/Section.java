package org.dailycode.pawon.entity;

/**
 * There are different purchase sections, like food, events, spa ... Technically
 * the purchase items, categories and sub categories are in each sections the
 * same. For a better maintenance a generalism was developed. To add a new
 * section please do like below and <ol> <li>add new entries to navigation.jsp
 * <li>add new entries to bean "urlMapping" in backend-dispatcher-servlet.xml
 * </ol> <b>CAUTION:</b> If a new section has to be added, append that one to
 * the end of the list. In a live system a change of the order would cause
 * conflicts
 */
public enum Section {

    EVENTS,
    FACILITIES,
    MISC,
    PHOTOGALLERY,
    PROMO,
    RESTAURANT;

    public static Section getSection(String pName) {
        if (pName == null) {
            return null;
        }
        for (Section section : Section.values()) {
            if (pName.toLowerCase().contains(section.name().toLowerCase())) {
                return section;
            }
        }
        return null;
    }
}
