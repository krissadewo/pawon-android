package org.dailycode.pawon;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.StreamCorruptedException;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;

import org.dailycode.pawon.entity.ItemOption;

import android.os.Environment;
import android.util.Log;

/**
 * Base class for those to maintain global application state without depend on
 * Activity class
 * 
 * @author kris
 * 
 */
public class App {

	public static final String PREF_SERVER = "pref_ip";
	public static final String PREF_PORT = "pref_port";
	public static final String PREF_TABLE = "pref_table";
	public static final String PREF_LANGUAGE = "pref_language";
	public static final String PREF_STATUS = "pref_status";
	private static App instance;
	private String hostURL;

	private App() {
	}

	public static App getInstance() {
		if (instance == null) {
			instance = new App();
		}
		return instance;
	}

	/**
	 * Get url of json data
	 * @param relativePath
	 * @return
	 * @throws MalformedURLException
	 * @throws URISyntaxException
	 */
	public URI getBaseJSONUrl(String relativePath) throws MalformedURLException, URISyntaxException {
		return createURI(getHostURL() + "/pawon/" + relativePath);
	}

	/**
	 * 
	 * @return ip,port and context path of the server
	 */
	public String getBaseUrl() {
		return getHostURL() + "/pawon";
	}

	/**
	 * 
	 * @return ip and port address
	 */
	public String getHostURL() {
		return hostURL;
	}

	public void setHostURL(String hostURL) {
		this.hostURL = hostURL;
	}

	/**
	 * Connect to service server
	 * 
	 * @param url
	 * @return
	 */
	public boolean isConnected() {
		HttpURLConnection urlConn = null;
		try {
			urlConn = (HttpURLConnection) new URL(App.getInstance().getBaseUrl()).openConnection();
			urlConn.setConnectTimeout(5000);
			urlConn.connect();
			if (urlConn.getResponseCode() == 200) {
				return true;
			}
		} catch (Exception e) {
			return false;
		} finally {
			urlConn.disconnect();
		}
		return false;
	}

	public URI createURI(String urlStr) throws MalformedURLException, URISyntaxException {
		URL url = null;
		URI uri = null;

		url = new URL(urlStr);
		uri = new URI(url.getProtocol(), url.getUserInfo(), url.getHost(), url.getPort(), url.getPath(),
				url.getQuery(), url.getRef());

		return uri;
	}

	public static String convertNumToIDR(Double value) {
		DecimalFormat decimalFormat = new DecimalFormat("#,###,##0.00");
		return decimalFormat.format(value);
	}

	@SuppressWarnings("unchecked")
	public List<ItemOption> getCurrentCheckedOptions() {
		FileInputStream fin;
		ObjectInputStream ois = null;

		File file = new File(Environment.getExternalStorageDirectory().getPath(), "pawon-temp.dat");

		if (file.exists() && file.isFile()) {
			try {
				fin = new FileInputStream(file);
				ois = new ObjectInputStream(fin);

				return (List<ItemOption>) ois.readObject();

			} catch (FileNotFoundException e) {
				e.printStackTrace();
			} catch (StreamCorruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			} catch (ClassNotFoundException e) {
				e.printStackTrace();
			} finally {
				try {
					ois.close();
				} catch (IOException e) {
					e.printStackTrace();
				}
			}

		}
		return new ArrayList<ItemOption>();

	}

	public void createCheckedOptions(List<ItemOption> itemOptions) {
		try {
			ObjectOutputStream os = new ObjectOutputStream(new FileOutputStream(new File(Environment
					.getExternalStorageDirectory().getPath(), "pawon-temp.dat")));

			os.writeObject(itemOptions);
			os.close();
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

	}

	public void deleteCheckedOptions() {
		File file = new File(Environment.getExternalStorageDirectory().getPath(), "pawon-temp.dat");

		if (file.exists()) {
			file.delete();
		}
	}

}
