package org.dailycode.pawon.preference;

import java.util.ArrayList;
import java.util.List;

import org.dailycode.pawon.App;
import org.dailycode.pawon.R;
import org.dailycode.pawon.activity.PawonActivity;
import org.dailycode.pawon.entity.Language;
import org.dailycode.pawon.helper.PawonTask;

import android.os.Bundle;
import android.preference.EditTextPreference;
import android.preference.ListPreference;
import android.preference.Preference;
import android.preference.Preference.OnPreferenceChangeListener;
import android.preference.PreferenceFragment;
import android.preference.SwitchPreference;

public class SettingPreference extends PreferenceFragment {

	private PawonActivity context;

	private SwitchPreference switchPreference;
	private ListPreference listPreference;

	public SettingPreference() {
	}

	public SettingPreference(PawonActivity context) {
		this.context = context;
	}

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setRetainInstance(false);
		// Load the preferences from an XML resource
		addPreferencesFromResource(R.xml.setting_preferences);

		EditTextPreference editTextTablePreference = (EditTextPreference) findPreference(App.PREF_TABLE);
		editTextTablePreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				new TableTask(context).execute(new Object[] { newValue });
				return true;
			}

		});
		editTextTablePreference.setSummary(context.getPrefTable());

		EditTextPreference editTextIPPreference = (EditTextPreference) findPreference(App.PREF_SERVER);
		editTextIPPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				new ConnectionTask(context).execute(new Object[] { (String) newValue, context.getPrefPort() });
				return true;
			}
		});
		editTextIPPreference.setSummary(context.getPrefServerIP());

		EditTextPreference editTextPortPreference = (EditTextPreference) findPreference(App.PREF_PORT);
		editTextPortPreference.setOnPreferenceChangeListener(new OnPreferenceChangeListener() {

			@Override
			public boolean onPreferenceChange(Preference preference, Object newValue) {
				new ConnectionTask(context).execute(new Object[] { context.getPrefServerIP(), (String) newValue });
				return true;
			}
		});
		editTextPortPreference.setSummary(context.getPrefPort());

		new ConnectionTask(context).execute(new Object[] { context.getPrefServerIP(), context.getPrefPort() });

		switchPreference = (SwitchPreference) findPreference(App.PREF_STATUS);
		switchPreference.setSelectable(false);
		listPreference = (ListPreference) findPreference(App.PREF_LANGUAGE);

	}

	class TableTask extends PawonTask<Object> {

		private String tableName;

		public TableTask(PawonActivity activity) {
			super(activity);
		}

		protected void onPreExecute() {
			super.onPreExecute();

		}

		@Override
		protected Boolean doInBackground(Object... params) {
			tableName = (String) params[0].toString().trim();
			if (context.getJSONService().getTableByName(tableName) == null) {
				return false;
			}

			return true;
		}

		protected void onPostExecute(Boolean result) {
			if (!result) {
				context.messageForDataNotExist(context);
			}
			super.onPostExecute(result);
		}
	}

	class LanguageTask extends PawonTask<Language> {

		private List<Language> languages = new ArrayList<Language>();

		public LanguageTask(PawonActivity activity) {
			super(activity);
		}

		protected void onPreExecute() {
			// super.onPreExecute();
		}

		@Override
		protected Boolean doInBackground(Object... params) {
			for (Language language : context.getJSONService().getAllLanguage()) {
				publishProgress(language);
			}

			return true;
		}

		protected void onProgressUpdate(Language... language) {
			languages.add(language[0]);
		}

		protected void onPostExecute(Boolean result) {
			String[] entries = new String[languages.size()];
			String[] entryValues = new String[languages.size()];

			int index = 0;
			for (Language language : languages) {
				entries[index] = language.getName();
				entryValues[index] = language.getId().toString();
				index++;
			}

			ListPreference listPreference = (ListPreference) findPreference("pref_language");
			listPreference.setEntries(entries);
			listPreference.setEntryValues(entryValues);
		}
	}

	/**
	 * This class a little bit different implementation from standard Pawon
	 * background thread class, because this class used to check connection
	 * independently for setting preference. This class doesn't check connection
	 * at {@link #onPreExecute(Void result)}, because it will check on
	 * {@link #doInBackground(Object...)}
	 * 
	 * @author kris
	 * 
	 */
	class ConnectionTask extends PawonTask<Boolean> {

		public ConnectionTask(PawonActivity activity) {
			super(activity);
		}

		@Override
		protected void onPreExecute() {
			context.showProgressLoadingDialog();
		}

		@Override
		protected Boolean doInBackground(Object... params) {
			App.getInstance().setHostURL("http://" + params[0] + ":" + params[1]);
			if (App.getInstance().isConnected()) {
				new LanguageTask(context).execute();
				return true;
			}
			return false;
		}

		@Override
		protected void onPostExecute(Boolean result) {
			if (result) {
				switchPreference.setChecked(true);
				listPreference.setEnabled(true);
			} else {
				listPreference.setEnabled(false);
				switchPreference.setChecked(false);
			}
			super.onPostExecute(result);
		}

	}

}
