package org.dailycode.pawon.preference;

import org.dailycode.pawon.R;

import android.os.Bundle;
import android.preference.PreferenceActivity;

public class DialogOptionPreference extends PreferenceActivity {

	@Override
	public void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		addPreferencesFromResource(R.xml.menu_option);

	}
}
