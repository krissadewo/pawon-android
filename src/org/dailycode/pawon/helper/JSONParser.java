package org.dailycode.pawon.helper;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URL;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

public class JSONParser {

	@Deprecated
	private JSONObject getJSONFromUrl(URL url) {
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url.toString());
		try {
			HttpResponse response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream content = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			return new JSONObject(builder.toString());
		} catch (JSONException e) {
			Log.e("JSON Parser", "Error parsing data " + e.toString());
			return null;
		}
	}

	/**
	 * Returning array off json data from pados service
	 * 
	 * @param url
	 * @return
	 * @throws JSONException
	 */
	private JSONArray getJSONArrayFromUrl(URL url) throws JSONException {
		StringBuilder builder = new StringBuilder();
		HttpClient client = new DefaultHttpClient();
		HttpGet httpGet = new HttpGet(url.toString());
		try {
			HttpResponse response = client.execute(httpGet);
			HttpEntity entity = response.getEntity();
			InputStream content = entity.getContent();
			BufferedReader reader = new BufferedReader(new InputStreamReader(content));
			String line;
			while ((line = reader.readLine()) != null) {
				builder.append(line);
			}
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		String json = builder.toString();
		if (builder.toString().startsWith("{") && builder.toString().endsWith("}")) {
			json = "[" + json + "]";
		}
		return new JSONArray(json);

	}

	/**
	 * Get data with prefix data exam :
	 * {"data":[{"id":1,"name":"English","code":
	 * null,"defaultLanguage":false,"active"
	 * :true,"image":"united_kingdom_flag.png"
	 * ,"statusDelete":false,"imageData":null,"languageCode":"FRENCH"}
	 * 
	 * @param url
	 * @return
	 * @throws JSONException
	 */
	public JSONArray getJSONData(URL url) throws JSONException {
		JSONObject c = getJSONArrayFromUrl(url).getJSONObject(0);
		String json = c.getString("data");		
		if (json.toString().startsWith("{") && json.toString().endsWith("}")) {
			json = "[" + json + "]";
		}
	
		return new JSONArray(json);
	}

	public String getJSONStatus(URL url) throws JSONException {
		JSONObject c = getJSONArrayFromUrl(url).getJSONObject(0);
		return c.getString("status");
	}

	/**
	 * Get single boolean data from json {"data":false}
	 * 
	 * @param url
	 * @return
	 * @throws JSONException
	 */
	public Boolean getJSONBoolean(URL url) throws JSONException {
		JSONObject c = getJSONArrayFromUrl(url).getJSONObject(0);
		return c.getBoolean("data");
	}

}
