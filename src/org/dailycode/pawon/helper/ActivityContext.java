package org.dailycode.pawon.helper;

/**
 * An Interface that create a context who implement this, a context mean an
 * activity, a context can be used for saving or creating view etc...
 * 
 * @author kris
 * 
 * @param <T>
 */
public interface ActivityContext<T> {

	T getContext();
}
