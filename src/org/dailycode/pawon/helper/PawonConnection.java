package org.dailycode.pawon.helper;

import java.net.HttpURLConnection;
import java.net.URL;

import org.dailycode.pawon.App;
import org.dailycode.pawon.activity.PawonActivity;

import android.os.AsyncTask;
import android.util.Log;

public class PawonConnection extends AsyncTask<Object, Integer, Boolean> {

	private PawonActivity activity;
	private boolean connected;
	private static PawonConnection instance;

	public PawonConnection(PawonActivity activity) {
		this.activity = activity;
		// TODO Auto-generated constructor stub
	}

	public static PawonConnection getInstance(PawonActivity activity) {
		if (instance == null) {
			instance = new PawonConnection(activity);
		}
		return instance;
	}

	protected void onPreExecute() {
		activity.showProgressLoadingDialog();

	}

	@Override
	protected Boolean doInBackground(Object... params) {
		HttpURLConnection urlConn = null;
		try {
			urlConn = (HttpURLConnection) new URL(App.getInstance().getBaseUrl()).openConnection();
			urlConn.setConnectTimeout(5000);
			urlConn.connect();
			if (urlConn.getResponseCode() == 200) {
				return true;
			}
		} catch (Exception e) {
			Log.d(this.getClass().getName(), e.toString());
		} finally {
			urlConn.disconnect();
		}

		return false;
	}

	protected void onPostExecute(Boolean result) {
		if (!result) {
			activity.closeProgressLoadingDialog();
			activity.messageForNotConnected();
		}

	}

	public boolean isConnected() {
		return connected;
	}

	public void setConnected(boolean connected) {
		this.connected = connected;
	}

}
