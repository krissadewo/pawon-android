package org.dailycode.pawon.helper;

import org.dailycode.pawon.App;
import org.dailycode.pawon.activity.PawonActivity;

import android.os.AsyncTask;

/**
 * This class handle how to standard background Thread used for derived class
 * Validation to internet work on {@link #onPreExecute()}. And at
 * {@link #onPostExecute(Void result)} close the progress loading dialog.
 * 
 * @author kris
 * @param <T>
 *            generic class of return value from background Thread
 * 
 */
public abstract class PawonTask<T> extends AsyncTask<Object, T, Boolean> {

	private PawonActivity context;

	public PawonTask(PawonActivity context) {
		this.context = context;
	}

	/**
	 * Start the progress loading, validated the network
	 */
	protected void onPreExecute() {
		context.showProgressLoadingDialog();
		if (!App.getInstance().isConnected()) {
			cancel(true);
			context.closeProgressLoadingDialog();
			context.messageForNotConnected();
		}

	}

	@Override
	protected Boolean doInBackground(Object... params) {
		return false;
	}

	/**
	 * Close the progress loading dialog
	 */
	protected void onPostExecute(Boolean result) {
		context.closeProgressLoadingDialog();
	}

}
